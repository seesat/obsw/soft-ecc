# Experimental Software ECC

This project is simple and experimental software implementation of the [FUEC-DAEC error correction code](https://riunet.upv.es/bitstream/handle/10251/121357/FinalVersion.pdf).

## Run Tests

```sh
cargo test
```

## Run Benchmarks

```sh
cargo bench
```

## Support

If you have any issues or suggestions, please use the the GitLab issue tracker of this project.

## Authors and acknowledgment

Initial implementation by Pau Kaifler (2023).

## License

This project uses the standard MIT license.

## Project status

This project is experimental and still under development!
