#[inline]
pub const fn calculate_code(x: u16) -> u16 {
    calculate_code_lut2(x)
}

#[inline]
pub const fn calculate_code_lut(x: u16) -> u16 {
    const CODE_TABLE: [u8; 1 << 16] = {
        let mut table = [0; 1 << 16];
        let mut i = 0;
        while i < table.len() {
            table[i] = calculate_code_count_immediate(i as u16) as u8;
            i += 1;
        }
        table
    };

    CODE_TABLE[x as usize] as u16
}

#[inline]
pub const fn calculate_code_lut2(x: u16) -> u16 {
    const CODE_TABLE_1: [u8; 1 << 8] = {
        let mut table = [0; 1 << 8];
        let mut i = 0;
        while i < table.len() {
            let x = i as u8;
            table[i] = ((x & 0b1001_1100).count_ones() as u8 & 1)
                | (((x & 0b0111_0010).count_ones() as u8 & 1) << 1)
                | (((x & 0b0100_1011).count_ones() as u8 & 1) << 2)
                | (((x & 0b1100_1000).count_ones() as u8 & 1) << 3)
                | (((x & 0b0000_1101).count_ones() as u8 & 1) << 4)
                | (((x & 0b0110_1010).count_ones() as u8 & 1) << 5)
                | (((x & 0b1011_0101).count_ones() as u8 & 1) << 6);
            i += 1;
        }
        table
    };
    const CODE_TABLE_2: [u8; 1 << 8] = {
        let mut table = [0; 1 << 8];
        let mut i = 0;
        while i < table.len() {
            let x = i as u8;
            table[i] = ((x & 0b1000_1001).count_ones() as u8 & 1)
                | (((x & 0b0101_0101).count_ones() as u8 & 1) << 1)
                | (((x & 0b1010_0011).count_ones() as u8 & 1) << 2)
                | (((x & 0b0100_1000).count_ones() as u8 & 1) << 3)
                | (((x & 0b1001_1001).count_ones() as u8 & 1) << 4)
                | (((x & 0b0110_0100).count_ones() as u8 & 1) << 5)
                | (((x & 0b0011_0010).count_ones() as u8 & 1) << 6);
            i += 1;
        }
        table
    };

    (CODE_TABLE_1[(x & 0xFF) as usize] ^ CODE_TABLE_2[((x >> 8) & 0xFF) as usize]) as u16
}

#[inline]
pub const fn calculate_code_count_immediate(x: u16) -> u16 {
    ((x & 0b1000_1001_1001_1100).count_ones() as u16 & 1)
        | (((x & 0b0101_0101_0111_0010).count_ones() as u16 & 1) << 1)
        | (((x & 0b1010_0011_0100_1011).count_ones() as u16 & 1) << 2)
        | (((x & 0b0100_1000_1100_1000).count_ones() as u16 & 1) << 3)
        | (((x & 0b1001_1001_0000_1101).count_ones() as u16 & 1) << 4)
        | (((x & 0b0110_0100_0110_1010).count_ones() as u16 & 1) << 5)
        | (((x & 0b0011_0010_1011_0101).count_ones() as u16 & 1) << 6)
}

#[inline]
pub const fn calculate_code_count_merge(x: u16) -> u16 {
    let c0 = (x & 0b1000_1001_1001_1100).count_ones() as u16 & 1;
    let c1 = (x & 0b0101_0101_0111_0010).count_ones() as u16 & 1;
    let c2 = (x & 0b1010_0011_0100_1011).count_ones() as u16 & 1;
    let c3 = (x & 0b0100_1000_1100_1000).count_ones() as u16 & 1;
    let c4 = (x & 0b1001_1001_0000_1101).count_ones() as u16 & 1;
    let c5 = (x & 0b0110_0100_0110_1010).count_ones() as u16 & 1;
    let c6 = (x & 0b0011_0010_1011_0101).count_ones() as u16 & 1;
    c0 | (c1 << 1) | (c2 << 2) | (c3 << 3) | (c4 << 4) | (c5 << 5) | (c6 << 6)
}

#[inline]
pub const fn calculate_code_naive(x: u16) -> u16 {
    #[inline]
    const fn f(x: u16, i: u16) -> u16 {
        (x >> i) & 1u16
    }
    let c0 = f(x, 0) ^ f(x, 4) ^ f(x, 7) ^ f(x, 8) ^ f(x, 11) ^ f(x, 12) ^ f(x, 13);
    let c1 = f(x, 1) ^ f(x, 3) ^ f(x, 5) ^ f(x, 7) ^ f(x, 9) ^ f(x, 10) ^ f(x, 11) ^ f(x, 14);
    let c2 = f(x, 0) ^ f(x, 2) ^ f(x, 6) ^ f(x, 7) ^ f(x, 9) ^ f(x, 12) ^ f(x, 14) ^ f(x, 15);
    let c3 = f(x, 1) ^ f(x, 4) ^ f(x, 8) ^ f(x, 9) ^ f(x, 12);
    let c4 = f(x, 0) ^ f(x, 3) ^ f(x, 4) ^ f(x, 7) ^ f(x, 12) ^ f(x, 13) ^ f(x, 15);
    let c5 = f(x, 1) ^ f(x, 2) ^ f(x, 5) ^ f(x, 9) ^ f(x, 10) ^ f(x, 12) ^ f(x, 14);
    let c6 = f(x, 2) ^ f(x, 3) ^ f(x, 6) ^ f(x, 8) ^ f(x, 10) ^ f(x, 11) ^ f(x, 13) ^ f(x, 15);
    c0 | (c1 << 1) | (c2 << 2) | (c3 << 3) | (c4 << 4) | (c5 << 5) | (c6 << 6)
}

const ERROR_TABLE: [(u16, u16); 1 << 7] = {
    let mut table = [(0, 0); 1 << 7];
    let patterns = [0b1u32, 0b11];
    let mut i = 0;
    while i < patterns.len() {
        let mut e = patterns[i];
        while e < 0x80_0000 {
            let s = ((e >> 16) as usize) ^ calculate_code(e as u16) as usize;
            table[s] = (e as u16, (e >> 16) as u16);
            e <<= 1;
        }
        i += 1;
    }
    table
};

#[inline]
pub const fn decode(c: u16, x: u16) -> Result<u16, ()> {
    let s = c ^ calculate_code(x);
    let e = ERROR_TABLE[s as usize].0;
    if s == 0 || e != 0 {
        Ok(e ^ x)
    } else {
        Err(())
    }
}

#[inline]
pub fn decode2(c: &mut u16, x: &mut u16) -> Result<(), ()> {
    let s = *c ^ calculate_code(*x);
    if s == 0 {
        Ok(())
    } else {
        let (ex, ec) = ERROR_TABLE[s as usize];
        if ex == 0 && ec == 0 {
            Err(())
        } else {
            *x ^= ex;
            *c ^= ec;
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn no_errors() {
        for u in 0..u16::MAX {
            assert_eq!(decode(calculate_code(u), u), Ok(u));
        }
    }

    #[test]
    fn correct_one_bit_errors() {
        for u in 0..u16::MAX {
            for k in 0..16 {
                let e = 1 << k;
                let mut x = u ^ e;
                let mut c = calculate_code(u);
                assert_eq!(decode2(&mut c, &mut x), Ok(()));
                assert_eq!(x, u);
                assert_eq!(c, calculate_code(u));
            }
        }
    }

    #[test]
    fn correct_one_bit_errors_ecc() {
        for u in 0..u16::MAX {
            for k in 0..7 {
                let e = 1 << k;
                let mut x = u;
                let mut c = calculate_code(u) ^ e;
                assert_eq!(decode2(&mut c, &mut x), Ok(()));
                assert_eq!(x, u);
                assert_eq!(c, calculate_code(u));
            }
        }
    }

    #[test]
    fn correct_two_bit_errors() {
        for u in 0..u16::MAX {
            for k in 0..15 {
                let e = 0b11 << k;
                assert_eq!(decode(calculate_code(u), u ^ e), Ok(u));
            }
        }
    }

    #[test]
    fn correct_two_bit_errors_ecc() {
        for u in 0..u16::MAX {
            for k in 0..6 {
                let e = 0b11 << k;
                let mut x = u;
                let mut c = calculate_code(u) ^ e;
                assert_eq!(decode2(&mut c, &mut x), Ok(()));
                assert_eq!(x, u);
                assert_eq!(c, calculate_code(u));
            }
        }
    }

    #[test]
    fn detect_three_bit_errors() {
        for u in 0..u16::MAX {
            for k in 0..14 {
                let e = 0b111 << k;
                assert_eq!(decode(calculate_code(u), u ^ e), Err(()));
            }
        }
    }

    #[test]
    fn detect_three_bit_errors_ecc() {
        for u in 0..u16::MAX {
            for k in 0..5 {
                let e = 0b111 << k;
                let mut x = u;
                let mut c = calculate_code(u) ^ e;
                assert_eq!(decode2(&mut c, &mut x), Err(()));
            }
        }
    }

    #[test]
    fn detect_four_bit_errors() {
        for u in 0..u16::MAX {
            for k in 0..14 {
                let e = 0b111 << k;
                assert_eq!(decode(calculate_code(u), u ^ e), Err(()));
            }
        }
    }

    #[test]
    fn detect_four_bit_errors_ecc() {
        for u in 0..u16::MAX {
            for k in 0..4 {
                let e = 0b1111 << k;
                let mut x = u;
                let mut c = calculate_code(u) ^ e;
                assert_eq!(decode2(&mut c, &mut x), Err(()));
            }
        }
    }
}
