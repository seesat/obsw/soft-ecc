use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rand::{Rng, SeedableRng};
use rand_xoshiro::Xoshiro256PlusPlus;

fn calculate_code(c: &mut Criterion) {
    let mut input = [0u8; 4096];
    Xoshiro256PlusPlus::seed_from_u64(0).fill(&mut input);
    let input: [u8; 4096] = black_box(input);
    let input_u16: [u16; 2048] = bytemuck::cast(input);

    let mut group = c.benchmark_group("Calculate Code");
    group.bench_function("Count LUT", |b| {
        b.iter(|| {
            for u in input_u16 {
                black_box(ecc_testing::calculate_code_lut(u));
            }
        })
    });
    group.bench_function("Count LUT2", |b| {
        b.iter(|| {
            for u in input_u16 {
                black_box(ecc_testing::calculate_code_lut2(u));
            }
        })
    });
    group.bench_function("Count Immediate", |b| {
        b.iter(|| {
            for u in input_u16 {
                black_box(ecc_testing::calculate_code_count_immediate(u));
            }
        })
    });
    group.bench_function("Count Merge", |b| {
        b.iter(|| {
            for u in input_u16 {
                black_box(ecc_testing::calculate_code_count_merge(u));
            }
        })
    });
    group.bench_function("Count Naive", |b| {
        b.iter(|| {
            for u in input_u16 {
                black_box(ecc_testing::calculate_code_naive(u));
            }
        })
    });
}

criterion_group!(benches, calculate_code);
criterion_main!(benches);
